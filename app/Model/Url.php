<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\QueryException;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;

class Url extends Model
{

    protected $primaryKey = 'id';

    protected $table = 'url';

    protected $fillable = ['description'];

    public static function store($value) {

        try {

            DB::table('url')->insert(['description' => $value->url ]);

        } catch (QueryException $exception) {

            Log::error('Erro ao tentar salvar url');

            return false;
        }

        return true;
    }

}
