<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Users extends Model
{
    protected $primaryKey = 'id';

    protected $table = 'users';

    protected $fillable = ['name','user_name','password'];

    public static function validateLogin($data) {

        $result = DB::table('users')
                ->where('username', $data->username)
                ->where('password', $data->password)
                ->first();

        return isset($result) ? $result : false;

    }

}
