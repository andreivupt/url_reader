<?php

namespace App\Http\Controllers;

use App\Model\Users;
use Illuminate\Http\Request;

class LoginController extends Controller
{
    public function index() {

        return view('login.login');

    }

    public function login(Request $request) {

        $request->validate([
            'username' => 'required|string',
            'password' => 'required|string'
        ]);

        $result = Users::validateLogin($request);

        if ($result) {
              return redirect()->route('index');
        } else {
            return redirect()->route('login')->with('error','Verifique suas credenciais de acesso');

        }
    }

}
