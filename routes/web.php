<?php

Route::get('/', 'LoginController@index')->name('login');
Route::post('authentication', 'LoginController@login')->name('authentication');

Route::post('urlStore', 'UrlController@getUrl')->name('store');
Route::get('index', 'UrlController@index')->name('index');

