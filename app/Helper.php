<?php

function status($param)
{
    $resp = [
        'N' => 'Não visualizada' ,
        'S' => 'Visualizada'
    ];

    return $resp[$param];
}
