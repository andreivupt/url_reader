<!DOCTYPE html>
<html lang="en">
<head>
    <title>Login V14</title>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/style.css') }}">
    <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/login/vendor/bootstrap/css/bootstrap.min.css')}} ">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/login/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/login/fonts/Linearicons-Free-v1.0.0/icon-font.min.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/login/vendor/animate/animate.css') }}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('assets/login/vendor/css-hamburgers/hamburgers.min.css')}}">
    <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{ asset('assets/login/css/util.css') }}">

</head>
<body>
@if ($message = Session::get('error'))
    <div class="alert alert-danger alert-block">
        <button type="button" class="close" data-dismiss="alert">×</button>
        <strong>{{ $message }}</strong>
    </div>
@endif
<div class="limiter">
    <div class="container-login100">
        <div class="wrap-login100 p-l-85 p-r-85 p-t-55 p-b-55">
            <form method="post" action="{{ route('authentication') }}" class="login100-form validate-form flex-sb flex-w">
                @csrf()

                <div class="col-md-12 m-b-36">
					<span class="login100-form-title p-b-32">Login</span>
                </div>
                <div class="col-md-12">
                    <span class="txt1 p-b-11">Usuário</span>
                </div>
                <div class="col-md-12 wrap-input100 validate-input" required data-validate = "Username is required">
                    <input class="form-control input100" type="text" name="username" >
                </div>

                <div class="col-md-12">
                    <span class="txt1 p-b-11">Senha</span>
                </div>
                <div class="col-md-12 wrap-input100 validate-input m-b-36" required>
                    <input class="form-control input100" type="password" name="password" >
                </div>

                <div class="col-md-12 container-login100-form-btn">
                    <button class="form-control login100-form-btn">Login</button>
                </div>
            </form>
        </div>
    </div>
</div>


<div id="dropDownSelect1"></div>



</body>
</html>
