<?php

namespace App\Http\Controllers;


use App\Http\Requests\UrlRequest;
use App\Model\Url;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class UrlController extends Controller
{

    public function index(Request $request) {

        $urls = Url::all();

        return view('system.index', compact('urls'));

    }

    public function getUrl(UrlRequest $request) {

        $result = Url::store($request);

        if ($result) {

            return redirect()->route('index')->with('success','Url inserida com sucesso');

        } else {

            return redirect()->back()->withErrors(['error', 'Url inválida']);
        }

    }

}
